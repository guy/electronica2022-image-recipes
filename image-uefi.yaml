{{ $architecture := or .architecture "amd64" }}
{{ $type := or .type "leaudio-central" }}
{{ $suite := or .suite "v2023pre" }}
{{ $ospack := or .ospack (printf "ospack_%s-%s-%s" $suite $architecture $type) }}
{{ $image := or .image (printf "apertis-%s-%s-%s" $suite  $type $architecture) }}

{{ $cmdline := or .cmdline "console=tty0 console=ttyS0,115200n8 rootwait rw quiet splash plymouth.ignore-serial-consoles fsck.mode=auto fsck.repair=yes snd-sof-pci.fw_path=\"intel/sof\"" }}

{{ $demopack := or .demopack "disabled" }}
{{ if eq $type "leaudio-peripheral" }}
{{ $demopack := "disabled" }}
{{ end }}

{{- $unpack := or .unpack "true" }}

architecture: {{ $architecture }}

actions:
{{- if eq $unpack "true" }}
  - action: unpack
    description: Unpack {{ $ospack }}
    compression: gz
    file: {{ $ospack }}.tar.gz
{{- end }}

#  - action: overlay
#    description: "Enable USB automount"
#    source: overlays/usb-automount-rules

  - action: image-partition
    imagename: {{ $image }}.img
{{ if eq $type "leaudio-peripheral" }}
    imagesize: 4G
{{ else }}
    imagesize: 15G
{{ end }}
    partitiontype: gpt

    mountpoints:
      - mountpoint: /
        partition: system
      - mountpoint: /boot/efi
        partition: EFI
      - mountpoint: /home
        partition: general_storage

    partitions:
      - name: EFI
        fs: vfat
        start: 0%
        end: 256M
        flags: [ boot ]
      - name: system
        fs: ext4
        start: 256M
{{ if eq $type "leaudio-peripheral" }}
        end: 3000M
{{ else }}
        end: 6000M
{{ end }}
      - name: general_storage
        fs: ext4
{{ if eq $type "leaudio-peripheral" }}
        start: 3000M
{{ else }}
        start: 6000M
{{ end }}
        end: 100%

  - action: filesystem-deploy
    description: Deploying ospack onto image
    append-kernel-cmdline: {{ $cmdline }}

  - action: apt
    description: "EFI bootloader"
    packages:
      - systemd-boot

  - action: run
    description: Install UEFI bootloader
    chroot: true
    command: bootctl --path=/boot/efi install

  # Avoid creation of machine-id entry for the image
  - action: run
    description: Create the default entry for booloader
    chroot: true
    command: mkdir /boot/efi/Default

  - action: apt
    description: Kernel and system packages for {{$architecture}}
    packages:
      - linux-image-{{$architecture}}
      - libgles2-mesa

#  - action: run
#    description: Switch to live APT repos
#    chroot: true
#    script: scripts/switch-apt-to-live.sh -r {{ $suite }}

  - action: run
    description: "Save installed package status"
    chroot: false
    command: gzip -c "${ROOTDIR}/var/lib/dpkg/status" > "${ARTIFACTDIR}/{{ $image }}.img.pkglist.gz"

  - action: run
    description: Cleanup /var/lib
    script: scripts/remove_var_lib_parts.sh

  # the clearing of machine-id can't be done before this point since
  # systemd-boot requires the machine-id to be set for reasons related to
  # dual-boot scenarios:
  # * to avoid conflicts when creating entries, see the `90-loaderentry` kernel
  #   install trigger
  # * to set the entries for the currently booted installation as default in
  #   the loader.conf generated by `bootctl install`
  #
  # in our image this is not useful, as the actual machine-id is supposed to be
  # uniquely generated on the first boot. however the impact is negligible, as
  # things still work albeit the code used to potentially disambiguate entries
  # doesn't match a real machine-id
  - action: run
    chroot: false
    description: "Empty /etc/machine-id so it's regenerated on first boot with an unique value"
    command: truncate -s0 "${ROOTDIR}/etc/machine-id"

  - action: run
    chroot: false
    description: Drop the systemd-boot random seed, see https://systemd.io/BUILDING_IMAGES/
    command: rm "${ROOTDIR}/boot/efi/loader/random-seed"

  # Add multimedia demo pack
  # Provide URL via '-t demopack:"https://images.apertis.org/media/multimedia-demo.tar.gz"'
  # to add multimedia demo files
  {{ if ne $demopack "disabled" }}
  # Use wget to get some insight about https://phabricator.collabora.com/T11930
  # TODO: Revert to a download action once the cause is found
  - action: run
    description: Download multimedia demo pack
    chroot: false
    command: wget --debug {{ $demopack }} -O "${ARTIFACTDIR}/multimedia-demo.tar.gz"

  - action: unpack
    description: Unpack multimedia demo pack
    compression: gz
    file: multimedia-demo.tar.gz

  - action: run
    description: Clean up multimedia demo pack tarball
    chroot: false
    command: rm "${ARTIFACTDIR}/multimedia-demo.tar.gz"
  {{ end }}

  - action: run
    description: Delete /usr/share/doc
    chroot: false
    command: rm -rf "${ROOTDIR}"/usr/share/doc/*

  - action: run
    description: List files on {{ $image }}
    chroot: false
    script: scripts/list-files "$ROOTDIR" | gzip > "${ARTIFACTDIR}/{{ $image }}.img.filelist.gz"

  - action: run
    description: Create block map for {{ $image }}.img
    postprocess: true
    command: bmaptool create "${ARTIFACTDIR}/{{ $image }}.img" > "${ARTIFACTDIR}/{{ $image }}.img.bmap"

  - action: run
    description: Compress {{ $image }}.img
    postprocess: true
    command: gzip -f "${ARTIFACTDIR}/{{ $image }}.img"

  - action: run
    description: Checksum for {{ $image }}.img.gz
    postprocess: true
    command: sha256sum "${ARTIFACTDIR}/{{ $image }}.img.gz" > "${ARTIFACTDIR}/{{ $image }}.img.gz.sha256"
