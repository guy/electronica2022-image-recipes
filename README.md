# electronica2022-image-recipes

This repository builds images for the Electronica 2022 demos using [debos](https://github.com/go-debos/debos).


See [README.ml-video-compression.md](README.ml-video-compression.md) for more information about
the recipes for the ML Video Compression demo.

See [README.leaudio.md](README.leaudio.md) for more information about the LE Audio images recipes for UP Squared 6000 boards.
